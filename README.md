# A2021软件基础

#### 本次作业介绍
实现一个命令行文本计数统计程序。能正确统计导入的纯英文txt文本中的字符数，单词数，句子数。

具体命令行界面要求如下：

命令模式： wc.exe [参数] [文件名]

例：wc.exe -c file.txt 统计字符数

#### 主要功能
1.wc.exe -w file.txt 统计单词数

2.wc.exe -s file.txt 统计句子数

3.wc.exe -c file.txt 统计字符数
#### 编写程序
采用一些函数来实现字符串的输入和识别，来实现正确统计导入的纯英文txt文本的字符数，单词书和句子数。

要注意必须为纯英文txt文本，不能含有数字等。

#### 文件列表及其相关说明
v0.1为空项目

v0.2为项目完成基础功能

v0.3暂时做不出来

1.txt为测试文件



#### 例程运行及其相关结果。
![输入图片说明](https://images.gitee.com/uploads/images/2021/1022/024350_8774a53a_9901606.png "5D8G3K@72CFH}LOVLE)(B)9.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/1022/024318_a1d316bb_9901606.png "在这里输入图片标题")
![输入图片说明](https://images.gitee.com/uploads/images/2021/1022/024438_adc05093_9901606.png "GWS(YRRKA_8HQCKJHFLS3RT.png")



